var adsConfig =
    {
        "viewports": {
            "desktop-xl": [1380, 0],
            "desktop-l": [1180, 0],
            "desktop": [1020, 0],
            "desktop-s": [980, 0],
            "tablet": [960, 0],
            "mobile": [768, 0],
            "all": [0, 0]
        },
        "sizes": {
            "button": [300, 100],
            "button-s": [300, 50],
            "buttonAds": [320, 100],
            "buttonAds-s": [320, 50],
            "fullSize": [468, 60],
            "largeLeaderBoard": [970, 90],
            "largeLeaderBoard-s": [728, 90],
            "rectangle": [300, 250],
            "billboard": [970, 250],
            "billboard-s": [728, 200],
            "superBanner": [970, 150],
            "superBanner-s": [728, 150],
            "sky": [300, 600],
            "sky-s": [160, 600],
            "wallpaper": [1920, 1080],
            "unknown_1": [1029, 90],
            "unknown_2": [1169, 90],
            "unknown_3": [640, 120]

        },
        "adunits": {
            "div-gpt-ad-stn_start_superbanner": {
                "slot": "stn_start_superbanner",
                "size-mapping": {
                    "all": [],
                    "desktop-xl": ["largeLeaderBoard-s", "superBanner", "unknown_1", "unknown_2"],
                    "desktop-l": ["largeLeaderBoard-s", "superBanner", "unknown_1"],
                    "desktop-s": ["largeLeaderBoard-s", "superBanner"],
                    "tablet": ["largeLeaderBoard-s", "unknown_3"],
                }
            },
            "div-gpt-ad-stn_start_msb": {
                "slot": "stn_start_superbanner",
                "size-mapping": {
                    "tablet": [],
                    "mobile": ["unknown_3"],
                    "all": ["buttonAds-s", "buttonAds"],
                }
            },
            "div-gpt-ad-stn_start_skyscraper_1": {
                "slot": "stn_start_skyscraper_1",
                "size-mapping": {
                    "all": [],
                    "desktop-xl": ["sky-s", "sky"],
                    "desktop-l": ["sky-s"],
                }
            },
            "div-gpt-ad-stn_start_msky": {
                "slot": "stn_start_skyscraper_1",
                "size-mapping": {
                    "desktop-l": [],
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_fs1": {
                "slot": "stn_hp_fs1",
                "size-mapping": {
                    "mobile": ["unknown_3"],
                    "all": ["buttonAds-s", "buttonAds"],
                }
            },
            "div-gpt-ad-stn_start_rectangle_1": {
                "slot": "stn_start_rectangle_1",
                "size-mapping": {
                    "all": [],
                    "desktop-l": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_start_mrec1": {
                "slot": "stn_start_rectangle_1",
                "size-mapping": {
                    "desktop-l": [],
                    "all": ["rectangle"],
                },
            },
            "stn_start_rectangle_2": {
                "slot": "div-gpt-ad-stn_start_rectangle_2",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "stn_start_rectangle_3": {
                "slot": "div-gpt-ad-stn_start_rectangle_3",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec4": {
                "slot": "stn_hp_rec4",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec5": {
                "slot": "stn_hp_rec5",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec6": {
                "slot": "stn_hp_rec6",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec7": {
                "slot": "stn_hp_rec7",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec8": {
                "slot": "stn_hp_rec8",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_hp_rec9": {
                "slot": "stn_hp_rec9",
                "size-mapping": {
                    "all": ["rectangle"],
                }
            },
            "div-gpt-ad-stn_wallpaper": {
                "slot": "stn_wallpaper",
                "size-mapping": {
                    "all": ["wallpaper"],
                }
            },
            "div-gpt-ad-stn_app_mobile_leaderboard_start": {
                "slot": "stn_app_mobile_leaderboard_start",
                "size-mapping": {
                    "all": ["buttonAds-s"],
                    "mobile": ["largeLeaderBoard-s"],
                    "desktop-l": [],
                }
            },
        }
    };

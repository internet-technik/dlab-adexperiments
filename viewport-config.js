const viewportsConfig = {
    "desktop-xl": [1380, 0],
    "desktop-l": [1180, 0],
    "desktop": [1020, 0],
    "desktop-s": [980, 0],
    "tablet": [960, 0],
    "mobile": [768, 0],
    "all": [0, 0]
};
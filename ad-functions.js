function AdFunctions() {
    function uniqueArrayOfArrays(array) {
        let uniqueArray = [];

        array.forEach(arrayElement => {
            const arrayElementString = arrayElement.join(",");
            let isUnique = true;

            uniqueArray.forEach(uniqueElement => {
                const uniqueElementString = uniqueElement.join(",");

                if (arrayElementString == uniqueElementString) {
                    isUnique = false;
                }
            });

            if (isUnique) {
                uniqueArray.push(arrayElement);
            }
        });

        return uniqueArray;
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function isTestTarget()
    {
        var queryAdsTest = getParameterByName('dlabAdsTest');
        if (queryAdsTest === 'on') localStorage.setItem('dlabAdsTest', 'on')
        if (queryAdsTest === 'off') localStorage.removeItem('dlabAdsTest')
        if ((queryAdsTest!== null && queryAdsTest !== 'off') || localStorage.getItem('dlabAdsTest')) {
            return true
        }
        return false
    }

    function checkDisplay(viewports) {
        var windowWidth = window.innerWidth;
        var display;

        for (const viewport in viewports) {
            let viewportWidth = viewports[viewport];
            if (Array.isArray(viewportWidth)) {
                [viewportWidth] = viewportWidth;
            }
            if (windowWidth >= parseInt(viewportWidth)) {
                display = viewport;
                break;
            }
        }

        return display;
    }

    this.initAds = function(data) {

        var viewportsConfig = data.viewportsConfig;
        var config = data.config;
        var onSuccess = data.onSuccess || function() {};
        var onEnableServices = data.onEnableServices || function() {};

        window.googletag = window.googletag || {cmd: []};

        googletag.cmd.push(function () {
            const slots = config.slots;
            const sizeMappings = config.sizeMappings;

            //googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs(true, true);

            if (window.dlab && window.dlab.adTargets) {
                var adTargets = window.dlab.adTargets;

                if (isTestTarget()) {
                    adTargets["test"] = '1';
                }

                for (var key in adTargets) {
                    var value = adTargets[key];
                    googletag.pubads().setTargeting(key, value.toString());
                }
            }

            onEnableServices({googletag: googletag});
            googletag.pubads().disableInitialLoad();
            googletag.enableServices();

            slots.forEach(slot => {
                if (!document.getElementById(slot.id)) return;
                const sizeMappingsForSlot = sizeMappings[slot.sizeMappingName];

                let allSizes = [];
                sizeMappingsForSlot.forEach(sizeMappingForSlot => {
                    allSizes.push(sizeMappingForSlot.sizes);
                });
                let distinctSizes = uniqueArrayOfArrays(allSizes.flat());


                let mapping = googletag.sizeMapping();
                sizeMappingsForSlot.forEach(sizeMappingForSlot => {
                    mapping.addSize(sizeMappingForSlot.viewPortSize, sizeMappingForSlot.sizes);
                });
                mapping = mapping.build();

                var newSlot = googletag.defineSlot(slot.path, distinctSizes, slot.id)
                    .defineSizeMapping(mapping)
                    .addService(googletag.pubads());

                if (slot.globalId) {
                    window[slot.globalId] = newSlot;
                }

                googletag.display(slot.id);
            });

            // Resize Part
            if (viewportsConfig) {
                var size = "";
                window.addEventListener("resize", function () {
                    if (size != checkDisplay(viewportsConfig)) {
                        size = checkDisplay(viewportsConfig);
                        googletag.pubads().refresh();
                    }
                });
            }

            onSuccess();

        });
    };
}

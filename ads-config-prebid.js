// For full documentation see
// https://github.com/eBayClassifiedsGroup/react-advertising/wiki/Configuration#size-mappings

// Config for Südtirol News
const config = {
    // GPT size mapping definitions (object). Ideally there is a sizeMapping for each Slot
    sizeMappings: {
        // Homepage
        SB_Desktop: [
            {
                viewPortSize: [980, 0],
                sizes: [[970, 150], [728, 150], [728, 90]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        SB_Tablet: [
            {
                viewPortSize: [960, 0],
                sizes: [],
            },
            {
                viewPortSize: [768, 0],
                sizes: [[728, 150], [728, 90]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[320, 100], [320, 50], [300, 100], [300, 50]],
            },
        ],
        SKY_Desktop: [
            {
                viewPortSize: [1380, 0],
                sizes: [[300, 600], [160, 600]],
            },
            {
                viewPortSize: [1180, 0],
                sizes: [[160, 600]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        SKY_Tablet: [
            {
                viewPortSize: [1180, 0],
                sizes: [],
            },
            {
                viewPortSize: [768, 0],
                sizes: [[728, 90]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[320, 50], [300, 50]],
            },
        ],
        SKY_REC_Desktop: [
            {
                viewPortSize: [980, 0],
                sizes: [[160, 600], [120, 600], [], [300, 250]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        REC_Desktop: [
            {
                viewPortSize: [980, 0],
                sizes: [[300, 250]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        REC_DesktopTablet: [
            {
                viewPortSize: [768, 0],
                sizes: [[300, 250]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        REC_TabletMobile: [
            {
                viewPortSize: [980, 0],
                sizes: [],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[300, 250]],
            },
        ],
        REC_NoTablet: [
            {
                viewPortSize: [960, 0],
                sizes: [[300, 250]],
            },
            {
                viewPortSize: [768, 0],
                sizes: [],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[300, 250]],
            },
        ],
        REC_OnlyDesktop: [
            {
                viewPortSize: [960, 0],
                sizes: [[300, 250]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        REC_OnlyTablet: [
            {
                viewPortSize: [960, 0],
                sizes: [],
            },
            {
                viewPortSize: [768, 0],
                sizes: [[300, 250]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        REC_OnlyMobile: [
            {
                viewPortSize: [768, 0],
                sizes: [],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[300, 250]],
            },
        ],
        REC: [
            {
                viewPortSize: [0, 0],
                sizes: [[300, 250]],
            },
        ],
        FS: [
            {
                viewPortSize: [1020, 0],
                sizes: [[640, 120]],
            },
            {
                viewPortSize: [960, 0],
                sizes: [],
            },
            {
                viewPortSize: [768, 0],
                sizes: [[728, 90]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [[320, 100], [320, 50], [300, 100], [300, 50]],
            },
        ],
        FS_Desktop: [
            {
                viewPortSize: [1020, 0],
                sizes: [[640, 120]],
            },
            {
                viewPortSize: [0, 0],
                sizes: [],
            },
        ],
        WALLPAPER: [
            {
                viewPortSize: [0, 0],
                sizes: [[1920, 1080]],
            },
        ],
    },

    // array of slot configurations
    slots: [
        // Homepage
        {
            // ID of the div
            id: "dlab-ad-H1-desktop",

            // Save slot in a window variable
            globalId: "superBannerSlot",

            // slot's ad unit path
            path: "/2273514/suedtirolnews/stn3_hp_sb",

            // name of sizeMapping that is defined for this slot
            sizeMappingName: "SB_Desktop",
        },
        {
            id: "dlab-ad-H1-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_sb",
            sizeMappingName: "SB_Tablet",
        },
        {
            id: "dlab-ad-H2-desktop",
            path: "/2273514/suedtirolnews/stn3_hp_sky",
            sizeMappingName: "SKY_Desktop",
        },
        {
            id: "dlab-ad-H2-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_sky",
            sizeMappingName: "SKY_Tablet",
        },
        {
            id: "dlab-ad-H3",
            path: "/2273514/suedtirolnews/stn3_hp_rec_1",
            sizeMappingName: "REC_DesktopTablet",
        },
        {
            id: "dlab-ad-H3-mobile",
            path: "/2273514/suedtirolnews/stn3_hp_rec_1",
            sizeMappingName: "REC_OnlyMobile",
        },
        {
            id: "dlab-ad-H4",
            path: "/2273514/suedtirolnews/stn3_hp_rec_2",
            sizeMappingName: "REC_OnlyDesktop",
        },
        {
            id: "dlab-ad-H4-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_rec_2",
            sizeMappingName: "REC_OnlyTablet",
        },
        {
            id: "dlab-ad-H4-mobile",
            path: "/2273514/suedtirolnews/stn3_hp_rec_2",
            sizeMappingName: "REC_OnlyMobile",
        },
        {
            id: "dlab-ad-H5-desktop",
            path: "/2273514/suedtirolnews/stn3_hp_rec_3",
            sizeMappingName: "REC_Desktop",
        },
        {
            id: "dlab-ad-H5-tablet-mobile",
            path: "/2273514/suedtirolnews/stn3_hp_rec_3",
            sizeMappingName: "REC_TabletMobile",
        },
        {
            id: "dlab-ad-H6",
            path: "/2273514/suedtirolnews/stn3_hp_fs",
            sizeMappingName: "FS",
        },
        {
            id: "dlab-ad-H7-desktop",
            path: "/2273514/suedtirolnews/stn3_hp_rec_4",
            sizeMappingName: "SKY_REC_Desktop",
        },
        {
            id: "dlab-ad-H7-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_rec_4",
            sizeMappingName: "REC_OnlyTablet",
        },
        {
            id: "dlab-ad-H7-mobile",
            path: "/2273514/suedtirolnews/stn3_hp_rec_4",
            sizeMappingName: "REC_OnlyMobile",
        },
        {
            id: "dlab-ad-H8",
            path: "/2273514/suedtirolnews/stn3_hp_rec_5",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-H9",
            path: "/2273514/suedtirolnews/stn3_hp_rec_6",
            sizeMappingName: "REC_NoTablet",
        },
        {
            id: "dlab-ad-H9-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_rec_6",
            sizeMappingName: "REC_OnlyTablet",
        },
        {
            id: "dlab-ad-H10",
            path: "/2273514/suedtirolnews/stn3_hp_rec_7",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-H11",
            path: "/2273514/suedtirolnews/stn3_hp_rec_8",
            sizeMappingName: "REC_NoTablet",
        },
        {
            id: "dlab-ad-H11-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_rec_8",
            sizeMappingName: "REC_OnlyTablet",
        },
        {
            id: "dlab-ad-H12",
            path: "/2273514/suedtirolnews/stn3_hp_rec_9",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-H13",
            path: "/2273514/suedtirolnews/stn3_hp_rec_10",
            sizeMappingName: "REC_NoTablet",
        },
        {
            id: "dlab-ad-H13-tablet",
            path: "/2273514/suedtirolnews/stn3_hp_rec_10",
            sizeMappingName: "REC_OnlyTablet",
        },
        {
            id: "dlab-ad-H14",
            path: "/2273514/suedtirolnews/stn3_hp_rec_11",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-WALLPAPER",
            path: "/2273514/suedtirolnews/stn3_wallpaper",
            sizeMappingName: "WALLPAPER",
        },
        // Default
        {
            id: "dlab-ad-S1-desktop",
            globalId: "superBannerSlot",
            path: "/2273514/suedtirolnews/stn3_default_sb",
            sizeMappingName: "SB_Desktop"
        },
        {
            id: "dlab-ad-S1-tablet",
            path: "/2273514/suedtirolnews/stn3_default_sb",
            sizeMappingName: "SB_Tablet"
        },
        {
            id: "dlab-ad-S2-desktop",
            path: "/2273514/suedtirolnews/stn3_default_sky",
            sizeMappingName: "SKY_Desktop",
        },
        {
            id: "dlab-ad-S2-tablet",
            path: "/2273514/suedtirolnews/stn3_default_sky",
            sizeMappingName: "SKY_Tablet",
        },
        {
            id: "dlab-ad-S4",
            path: "/2273514/suedtirolnews/stn3_default_rec_1",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-S4-desktop",
            path: "/2273514/suedtirolnews/stn3_default_rec_1",
            sizeMappingName: "REC_Desktop",
        },
        {
            id: "dlab-ad-S4-tablet",
            path: "/2273514/suedtirolnews/stn3_default_rec_1",
            sizeMappingName: "REC_TabletMobile",
        },
        {
            id: "dlab-ad-S5-desktop",
            path: "/2273514/suedtirolnews/stn3_default_rec_2",
            sizeMappingName: "REC_Desktop",
        },
        {
            id: "dlab-ad-S5",
            path: "/2273514/suedtirolnews/stn3_default_rec_2",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-S5-tablet-mobile",
            path: "/2273514/suedtirolnews/stn3_default_rec_2",
            sizeMappingName: "REC_TabletMobile",
        },
        {
            id: "dlab-ad-S11",
            path: "/2273514/suedtirolnews/stn3_default_rec_3",
            sizeMappingName: "REC",
        },
        {
            id: "dlab-ad-S6-desktop",
            path: "/2273514/suedtirolnews/stn3_default_fs_1",
            sizeMappingName: "FS_Desktop",
        },
        {
            id: "dlab-ad-S6",
            path: "/2273514/suedtirolnews/stn3_default_fs_1",
            sizeMappingName: "FS",
        },
        {
            id: "dlab-ad-S7",
            path: "/2273514/suedtirolnews/stn3_default_fs_2",
            sizeMappingName: "FS",
        },
        {
            id: "dlab-ad-S8",
            path: "/2273514/suedtirolnews/stn3_default_fs_3",
            sizeMappingName: "FS",
        },
        {
            id: "dlab-ad-S9",
            path: "/2273514/suedtirolnews/stn3_default_fs_4",
            sizeMappingName: "FS",
        },
        {
            id: "dlab-ad-S10",
            path: "/2273514/suedtirolnews/stn3_default_fs_5",
            sizeMappingName: "FS",
        },
    ]
};
